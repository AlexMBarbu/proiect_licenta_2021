﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectLicenta_Console
{
    class InputOutput
    {
        public string filePath;
        public string directoryPath;
        public string fileName;
        public string fileFormat;

        public InputOutput()
        {

        }

        public string ReverseString(string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public void GetEverythingFromFilePath(string filePath)
        {
            directoryPath = "";
            fileName = "";
            fileFormat = "";

            bool FirstSlash = false;
            bool FirstPoint = false;
            for (int i = filePath.Length-1; i>=0; i--)
            {
                //Split the file path into directory path, file name, file format
                if (filePath[i] == '\\')
                {
                    FirstSlash = true;
                }
                if (filePath[i] == '.')
                {
                    FirstPoint = true;
                }

                if (FirstSlash == false)
                {
                    if(FirstPoint == false)
                    {
                        fileFormat += filePath[i];
                    }
                    else
                    {
                        fileName += filePath[i];
                    }
                }
                else
                {
                    directoryPath += filePath[i];
                }
            }

            fileFormat = ReverseString(fileFormat);
            fileName = ReverseString(fileName);
            fileName = fileName.Remove(fileName.Length-1);
            directoryPath = ReverseString(directoryPath);
        }

        public void GetFilePath()
        {
            Console.WriteLine("Insert the filePath:\n");
            filePath = Console.ReadLine();
            GetEverythingFromFilePath(filePath);
        }
    }
}

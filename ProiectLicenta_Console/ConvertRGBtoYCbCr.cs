﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectLicenta_Console
{
    public class ConvertRGBtoYCbCr
    {
        public static float ConvertRGBToY(int R, int G, int B)
        {
            return (float)(0.2989 * R + 0.5866 * G + 0.1145 * B);
        }

        public static float ConvertRGBToCb(int R, int G, int B)
        {
            return (float)(-0.1687 * R - 0.3313 * G + 0.5000 * B);
        }

        public static float ConvertRGBToCr(int R, int G, int B)
        {
            return (float)(0.5000 * R - 0.4184 * G - 0.0816 * B);
        }

        public static float ConvertYCbCrToR(int Y, int Cb, int Cr)
        {
            return Math.Max(1.0f, (float)(Y + 0.0000 * Cb + 1.4022 * Cr));
        }

        public static float ConvertYCbCrToG(int Y, int Cb, int Cr)
        {
            return Math.Max(1.0f, (float)(Y - 0.3456 * Cb - 0.7145 * Cr));
        }

        public static float ConvertYCbCrToB(int Y, int Cb, int Cr)
        {
            return Math.Max(1.0f, (float)(Y + 1.7710 * Cb + 0.0000 * Cr));
        }
    }
}

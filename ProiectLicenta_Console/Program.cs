﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ProiectLicenta_Console
{
    class Program
    {
        public static byte[] GetByte(ImageObj imageObj, InputOutput inputOutput)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                ImageCodecInfo jpegEncoder = imageObj.GetEncoder(ImageFormat.Jpeg);
                Encoder myEncoder = Encoder.Quality;
                EncoderParameters myEncoderParameters = new EncoderParameters(1);
                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                myEncoderParameters.Param[0] = myEncoderParameter;
                imageObj.image.Save(ms, jpegEncoder, myEncoderParameters);
                return ms.ToArray();
            }
        }

        static void Main(string[] args)
        {
            ImageObj imageObj = new ImageObj();
            InputOutput inputOutput = new InputOutput();

            inputOutput.GetFilePath();

            imageObj.OpenImageFromFilePath(inputOutput.filePath);

            imageObj.CreateBitmapFromImage();

            //Color newPixelColor = Color.FromArgb(255, 255, 0, 0);

            //imageObj.bitmap.SetPixel(0, 0, newPixelColor);

            //imageObj.SaveBitmap(imageObj.bitmap, inputOutput.directoryPath, "TEEEEEEEE", "JPEG");





            /*byte[] img = GetByte(imageObj, inputOutput);
            byte[] byteArray = File.ReadAllBytes(inputOutput.filePath);

            Console.WriteLine(img.Length + " " + byteArray.Length);*/




            Bitmap bmpY = (Bitmap)imageObj.bitmap.Clone();
            Bitmap bmpCb = (Bitmap)imageObj.bitmap.Clone();
            Bitmap bmpCr = (Bitmap)imageObj.bitmap.Clone();

            int R=0, G=0, B = 0;

            for (int i=0; i<imageObj.bitmap.Width;i++)
            {
                for (int j = 0; j < imageObj.bitmap.Height; j++)
                {
                    R = imageObj.bitmap.GetPixel(i, j).R;
                    G = imageObj.bitmap.GetPixel(i, j).G;
                    B = imageObj.bitmap.GetPixel(i, j).B;

                    int Y = (int)ConvertRGBtoYCbCr.ConvertRGBToY(R, G, B);
                    int Cb = (int)ConvertRGBtoYCbCr.ConvertRGBToCb(R, G, B);
                    int Cr = (int)ConvertRGBtoYCbCr.ConvertRGBToCr(R, G, B);

                    //The Y image
                    R = (int)ConvertRGBtoYCbCr.ConvertYCbCrToR(Y, 0, 0);
                    G = (int)ConvertRGBtoYCbCr.ConvertYCbCrToG(Y, 0, 0);
                    B = (int)ConvertRGBtoYCbCr.ConvertYCbCrToB(Y, 0, 0);
                    bmpY.SetPixel(i, j,Color.FromArgb(255,R,G,B));

                    //The Cb image
                    R = (int)ConvertRGBtoYCbCr.ConvertYCbCrToR(128, Cb, 0);
                    G = (int)ConvertRGBtoYCbCr.ConvertYCbCrToG(128, Cb, 0);
                    B = (int)ConvertRGBtoYCbCr.ConvertYCbCrToB(128, Cb, 0);
                    bmpCb.SetPixel(i, j, Color.FromArgb(255, R, G, B));

                    //The Cr image
                    R = (int)ConvertRGBtoYCbCr.ConvertYCbCrToR(128, 0, Cr);
                    G = (int)ConvertRGBtoYCbCr.ConvertYCbCrToG(128, 0, Cr);
                    B = (int)ConvertRGBtoYCbCr.ConvertYCbCrToB(128, 0, Cr);
                    bmpCr.SetPixel(i, j, Color.FromArgb(255, R, G, B));
                }
            }



            imageObj.SaveBitmap(bmpY, inputOutput.directoryPath, "Y", "PNG");
            imageObj.SaveBitmap(bmpCb, inputOutput.directoryPath, "Cb", "PNG");
            imageObj.SaveBitmap(bmpCr, inputOutput.directoryPath, "Cr", "PNG");

            //DO SOME WORK WITH THE IMAGE

            //export the result and delete the auxiliary



            /*for(int i=img.Length-1; i>=0; i--)
            {
                Console.WriteLine(img[i]);
            }*/

            //imageObj.SaveByteArray(img, inputOutput.directoryPath, "newJPEG", "JPEG");
        }
    }
}

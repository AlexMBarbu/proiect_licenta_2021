﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectLicenta_Console
{
    class ImageObj
    {
        public Image image;
        public Bitmap bitmap;

        public ImageObj()
        {

        }

        public void OpenImageFromFilePath(string filePath)
        {
            image = Image.FromFile(filePath);
        }

        public void CreateBitmapFromImage()
        {
            bitmap = new Bitmap(image);
        }

        public int GetBitmapHeight()
        {
            return bitmap.Height;
        }

        public int GetBitmapWidth()
        {
            return bitmap.Width;
        }

        public ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public void SaveBitmap(Bitmap bitmapToSave, string directoryToSave, string fileName, string format)
        {
            switch (format)
            {
                case "PNG":
                    bitmapToSave.Save(directoryToSave + fileName + ".png", ImageFormat.Png);
                    break;
                case "JPEG":
                case "JPG":
                    ImageCodecInfo jpegEncoder = GetEncoder(ImageFormat.Jpeg);
                    System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);
                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                    myEncoderParameters.Param[0] = myEncoderParameter;
                    if (format.Equals("JPEG"))
                    {
                        bitmapToSave.Save(directoryToSave + fileName + ".jpeg", jpegEncoder, myEncoderParameters);
                    }
                    else
                    {
                        bitmapToSave.Save(directoryToSave + fileName + ".jpg", jpegEncoder, myEncoderParameters);
                    }
                    break;
                case "BMP":
                    bitmapToSave.Save(directoryToSave + fileName + ".bmp", ImageFormat.Bmp);
                    break;
                default:
                    bitmapToSave.Save(directoryToSave + fileName + ".bmp", ImageFormat.Bmp);
                    break;
            }
        }

        public Image ByteArrayToImage(byte[] byteArrayToConvert)
        {
            MemoryStream memoryStream = new MemoryStream(byteArrayToConvert);
            return Image.FromStream(memoryStream);
        }
        
        public byte[] ImageToByteArray(Image imageToConvert)
        {
            MemoryStream memoryStream = new MemoryStream();
            imageToConvert.Save(memoryStream, ImageFormat.Jpeg);
            return memoryStream.ToArray();
        }

        public void SaveByteArray(byte[] byteArrayToSave, string directoryToSave, string fileName, string format)
        {
            File.WriteAllBytes(directoryToSave + fileName+".jpg", byteArrayToSave);
        }
    }
}
